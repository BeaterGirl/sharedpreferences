package com.ia.sharedpreferencesmanager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       /* final SharedPreferences sharedPreferences = getPreferences(Context.MODE_PRIVATE); //regresa el acceso al archivo del preferences*/
        final EditText editTextName = this.findViewById(R.id.edit_text_name);
        final EditText editTextLastName = this.findViewById(R.id.edit_text_last_name);
        final EditText editTextEmail = this.findViewById(R.id.edit_text_email);
        final EditText editTextAge = this.findViewById(R.id.edit_text_age);
        final Button buttonSave = this.findViewById(R.id.button_save);

        final CheckBox cbRecordarUsuario = this.findViewById(R.id.cb_Recordar_Usuario);
        boolean rememberUser = SharedPreferencesManager.rememberUser(this);
        cbRecordarUsuario.setChecked(rememberUser);

        cbRecordarUsuario.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean itsChecked) {
                SharedPreferencesManager.rememberUser(MainActivity.this, itsChecked);
            }
        });

        if(SharedPreferencesManager.rememberUser(MainActivity.this)){
           User user = SharedPreferencesManager.getUserData(MainActivity.this);
           editTextName.setText(user.getName());
        }

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = editTextName.getText().toString().trim();
                String lastName = editTextLastName.getText().toString().trim();
                String email = editTextEmail.getText().toString().trim();
                String ageValue = editTextAge.getText().toString().trim();
                int age = Integer.valueOf(ageValue);
                //TODO Guardar datos
                User user = new User(name, lastName, email, age);
                SharedPreferencesManager.saveUserData(MainActivity.this, user);

                Intent intent = new Intent(MainActivity.this, MostrarDatosActivity.class);
                startActivity(intent);
                finish();
            }
        });
       /* textViewWelcomeMessage.setVisibility(isDateSave ? View.INVISIBLE : View.VISIBLE);*/
    }
}
