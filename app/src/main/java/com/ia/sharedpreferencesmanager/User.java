package com.ia.sharedpreferencesmanager;

/**
 * Created by amartineza on 3/5/2018.
 */

public class User {

    private String name;
    private String lasName;
    private String email;
    private int age;

    public User(String name, String lasName, String email, int age) {
        this.name = name;
        this.lasName = lasName;
        this.email = email;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLasName() {
        return lasName;
    }

    public void setLasName(String lasName) {
        this.lasName = lasName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
