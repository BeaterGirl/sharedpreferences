package com.ia.sharedpreferencesmanager;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by amartineza on 3/5/2018.
 */

public class SharedPreferencesManager {

    private static final String GLOBAL_PREFERENCES = "GLOBAL_PREFERENCES";
    private static SharedPreferences sharedPreferences;

    private SharedPreferencesManager() {
    }

    private static String getPreferencesKey(Context context, String key) {
        return String.format("%s.%s", context.getPackageName(), key); //obtener un nombre unico de archivo
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(getPreferencesKey(context, GLOBAL_PREFERENCES), Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }

    static User getUserData(Context context) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        String name = sharedPreferences.getString("name", "");
        String lastName = sharedPreferences.getString("lastName", "");
        String email = sharedPreferences.getString("email", "");
        int age = sharedPreferences.getInt("age", 0);
        return new User(name, lastName, email, age);
    }

    static void saveUserData(Context context, User user) { //escribo todos los datos en el shared
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("name", user.getName());
        editor.putString("lastName", user.getLasName());
        editor.putString("email", user.getEmail());
        editor.putInt("age", user.getAge());
        editor.putBoolean("is_user_data_save", true);
        editor.apply();
    }

    static boolean isUserDataSave(Context context) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        return sharedPreferences.getBoolean("is_user_data_save", false);
    }

    static void clearUserData(Context context) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        if (sharedPreferences.getBoolean("is_user_remember", false)) {  //Guardar solo el nombre
            editor.putString("lastName", "");
            editor.putString("email", "");
            editor.putInt("age", 0);
        } else { //borrar todo
            editor.clear();
        }
        editor.putBoolean("is_user_data_save", false);
        editor.apply();
    }

    static boolean rememberUser(Context context) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        return sharedPreferences.getBoolean("is_user_remember", false);
    }

    static void rememberUser(Context context, boolean user) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("is_user_remember", user);
        editor.apply();
    }
}
